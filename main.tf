

##############################################################################
# IBM Cloud Provider
##############################################################################

provider ibm {
  ibmcloud_api_key      = var.ibmcloud_api_key
  region                = var.ibm_region
  generation            = var.generation
  ibmcloud_timeout      = 60
}

##############################################################################


##############################################################################
# VPC Data Block
##############################################################################

data ibm_is_vpc vpc {
    name = var.vpc_name
}

##############################################################################


##############################################################################
# Resource Group for VSI and Load Balancer
##############################################################################

data ibm_resource_group group {
  name = var.resource_group
}

##############################################################################

##############################################################################
# Get IDs for subnets
##############################################################################

data ibm_is_subnet subnet {
  count      = length(var.subnet_ids)
  identifier = var.subnet_ids[count.index]
}

##############################################################################


##############################################################################
# Create VSI
##############################################################################

module vsi {
  source            = "./vsi_module"
  unique_id         = var.unique_id
  image             = var.image
  ssh_public_key    = var.ssh_public_key  
  machine_type      = var.machine_type
  vsi_per_subnet    = var.vsi_per_subnet
  enable_fip        = var.enable_fip  
  resource_group_id = data.ibm_resource_group.group.id
  vpc_id            = data.ibm_is_vpc.vpc.id
  subnet_zones      = data.ibm_is_subnet.subnet.*.zone
  subnet_ids        = var.subnet_ids
  volumes           = var.volumes
}

##############################################################################
